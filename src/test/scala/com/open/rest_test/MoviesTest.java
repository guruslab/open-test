package com.open.rest_test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.open.test.Movies;
import com.open.test.OpenTestApplication;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = OpenTestApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MoviesTest {
	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port = 8082;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}
	//**************************  Rest Test   *******************
	@Test
	public void testAddMovies() {
		List<Movies> moviesList = new ArrayList<Movies>();
		JSONParser parser = new JSONParser();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			File resource = new ClassPathResource(
				      "input.json").getFile();
//			JSONArray jSONArray = (JSONArray) parser.parse(new FileReader("C:\\Users\\mm955y\\Documents\\Basha\\Project Sample\\input.json"));
			JSONArray jSONArray = (JSONArray) parser.parse(new FileReader(resource.getAbsolutePath()));
			System.out.println(jSONArray.get(0).toString());
			int objCount=jSONArray.size();
			for(int i=0; i<objCount; i++) {
				System.out.println(jSONArray.get(i));
				Movies movies = objectMapper.readValue(jSONArray.get(i).toString(), Movies.class);
				movies.set_id(ObjectId.get());
				moviesList.add(movies);
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		for(Movies movies : moviesList) {
			ResponseEntity<Movies> postResponse = restTemplate.postForEntity(getRootUrl() + "/movies/", movies, Movies.class);
			System.out.println(postResponse.getBody().toString());
			assertEquals(HttpStatus.OK, postResponse.getStatusCode());
		}
		
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		System.out.println("Entire Url: "+getRootUrl() + "/movies");
		ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/movies/",
				HttpMethod.GET, entity, String.class);
		System.out.println("Output: "+response.getBody().toString());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	
	@Test
	public void testGetAllMovies() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		System.out.println("Entire Url: "+getRootUrl() + "/movies");
		ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/movies/",
				HttpMethod.GET, entity, String.class);
		System.out.println("Output: "+response.getBody().toString());
		assertEquals(HttpStatus.OK, response.getStatusCode());

	}


}
