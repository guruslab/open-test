package com.open.test.perf

import io.gatling.core.scenario.Simulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import java.util.Date
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit

class ApiGatlingSimulationTest extends Simulation {

  val scn = scenario("GetMovies").repeat(10, "n") 
		  {
      exec(
        http("GetMovies")
          .get("https://fng-discovery-b-qp.foxplay.com/fox/restapi/container?apiKey=qwerty&pageSize=1")
          .check(status.is(200))
      )   
}
		 /*{
        exec(
          http("GetMovies")
            .post("https://fng-discovery-b-qp.foxplay.com/fox/restapi/container?apiKey=qwerty&pageSize=1")
            .header("Content-Type", "application/json")
            .body(StringBody("""{"firstName":"John${n}","lastName":"Smith${n}","birthDate":"1980-01-01", "address": {"country":"pl","city":"Warsaw","street":"Test${n}","postalCode":"02-200","houseNo":${n}}}"""))
            .check(status.is(200))
        ).pause(Duration.apply(5, TimeUnit.MILLISECONDS))    
  }.repeat(10, "n")*/
  
  setUp(scn.inject(atOnceUsers(30))).maxDuration(FiniteDuration.apply(1, "minutes"))
  
}