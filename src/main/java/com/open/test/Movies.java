package com.open.test;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Movies {
	@Id
	public ObjectId _id;

	public String Title;
	public Integer Year;
	public Integer Runtime;
	public String Rated;
	public String Released;
	public String Type;
	public String Genre;
	public String Director;
	public String Writer;
	public String Actors;
	public String Plot;
	public String Language;
	public String Country;
	public String Awards;
	public String Poster;

	// Constructors
	public Movies() {}

	public Movies(ObjectId _id, String Title, Integer Year, String Rated,String Released,String Type,Integer Runtime,String Genre,String Director,String Writer,String Actors, String Plot, String Language, String Country, String Awards, String Poster ) 
	{
		this._id = _id;
		this.Title = Title;
		this.Year = Year;
		this.Rated = Rated;
		this.Released = Released;
		this.Type = Type;
		this.Runtime = Runtime;
		this.Genre = Genre;
		this.Director = Director;
		this.Writer = Writer;
		this.Actors = Actors;
		this.Plot = Plot;
		this.Country = Country;
		this.Awards = Awards;
		this.Poster = Poster;
	}

	// ObjectId needs to be converted to string
	public String get_id() { return _id.toHexString(); }
	public void set_id(ObjectId _id) { this._id = _id; }

	public String getTitle() { return Title; }
	public void setTitle(String Title) { this.Title = Title; }

	public Integer getYear() { return Year; }
	public void setYear(Integer Year) { this.Year = Year; }

	public String getRated() { return Rated; }
	public void setRated(String Rated) { this.Rated = Rated; }

	public String getReleased() { return Released; }
	public void setReleased(String Released) { this.Released = Released; }

	public String getType() { return Type; }
	public void setType(String Type) { this.Type = Type; }

	public Integer getRunTime() { return Runtime; }
	public void setRuntime(Integer Runtime) { this.Runtime = Runtime; }

	public String getGenre() { return Genre; }
	public void setGenre(String Genre) { this.Genre = Genre; }

	public String getDirector() { return Director; }
	public void setDirector(String Director) { this.Director = Director; }

	public String getWriter() { return Writer; }
	public void setWriter(String Writer) { this.Writer = Writer; }

	public String getActors() { return Actors; }
	public void setActors(String Actors) { this.Actors = Actors; }

	public String getPlot() { return Plot; }
	public void setPlot(String Plot) { this.Plot = Plot; }

	public String getCountry() { return Country; }
	public void setCountry(String Country) { this.Country = Country; }

	public String getAwards() { return Awards; }
	public void setAwards(String Awards) { this.Awards = Awards; }

	public String getPoster() { return Poster; }
	public void setPoster(String Poster) { this.Poster = Poster; }

}
