package com.open.test;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface Repository extends MongoRepository<Movies, String> {
Movies findBy_id(ObjectId _id);

}
